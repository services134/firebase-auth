import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import * as admin from 'firebase-admin';

@Injectable()
export class AppService {
  app: admin.app.App;
  constructor(private readonly configService: ConfigService) {
    this.app = admin.initializeApp({
      credential: admin.credential.cert(configService.get('firebase')),
    });
  }

  profile() {
    return this.app.auth().verifySessionCookie('sessionCookie', true);
  }

  createSessionCookie(idToken, expiresIn) {
    return this.app.auth().createSessionCookie(idToken, { expiresIn });
  }
}
