import { All, Controller, Get, Post, Render, Req, Res } from '@nestjs/common';
import { AppService } from './app.service';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  @Render('index')
  root() {
    return { message: 'Hello world!' };
  }

  @Get('/login')
  @Render('login')
  login() {
    return { message: 'Hello world!' };
  }

  @Get('/signup')
  @Render('signup')
  signup() {
    return { message: 'Hello world!' };
  }

  @Get('/login/email')
  @Render('auth/email')
  loginEmail() {
    return { message: 'Login with email' };
  }

  @Get('/login/phone')
  @Render('auth/phone')
  loginPhone() {
    return { message: 'Login with phone' };
  }

  @All('*')
  xsrfToken(@Req() req, @Res() res) {
    res.cookie('XSRF-TOKEN', req.csrfToken());
    res.locals.csrfToken = req.csrfToken();
    res.end();
  }

  @Get('/profile')
  profile(@Req() req, @Res() res) {
    console.log(req.cookies);
    const sessionCookie = req.cookies.session || '';
    if (sessionCookie) {
      this.appService
        .profile()
        .then((decodedClaims) => {
          console.log(decodedClaims);
          return res.render('profile', { user: decodedClaims });
        })
        .catch((error) => {
          return res.redirect('/login');
        });
    }
  }

  @Post('/sessionLogin')
  sessionLogin(@Req() req, @Res() res) {
    const idToken = req.body.idToken.toString();
    const expiresIn = 60 * 60 * 24 * 5 * 1000;
    this.appService
      .createSessionCookie(idToken, expiresIn)
      .then((sessionCookie) => {
        const options = { maxAge: expiresIn, httpOnly: true, secure: true };
        res.cookie('session', sessionCookie, options);
        return res.end(JSON.stringify({ status: 'success' }));
      })
      .catch((error) => {
        res.status(401).send('UNAUTHORIZED REQUEST!');
      });
  }

  @Get('/sessionLogout')
  sessionLogout(@Req() req, @Res() res) {
    res.clearCookie('session');
    res.redirect('/login');
  }
}
