import { Injectable, NestMiddleware } from '@nestjs/common';
import * as firebase from 'firebase-admin';
import { Response } from 'express';
import { ConfigService } from '@nestjs/config';

@Injectable()
export class PreAuthMiddleware implements NestMiddleware {
  private defaultApp: any;

  constructor(private readonly configService: ConfigService) {
    this.defaultApp = firebase.initializeApp({
      credential: firebase.credential.cert(this.configService.get('firebase')),
      // databaseURL: 'https://<your-firebase-project>.firebaseio.com',
    });
  }

  use(req: any, res: any, next: () => void) {
    const token = req.headers.authorization;
    if (token !== null && token !== '') {
      this.defaultApp
        .auth()
        .verifyIdToken(token.replace('Bearer ', ''))
        .then(async (decodedToken) => {
          req.user = { email: decodedToken.email };
          next();
        })
        .catch((error) => {
          console.log(error);
          this.accessDenied(req.url, res);
        });
    } else {
      next();
    }
  }

  private accessDenied(url: string, res: Response) {
    res.status(403).json({
      statusCode: 403,
      timestamp: new Date().toISOString(),
      path: url,
      message: 'Access denied',
    });
  }
}
