import { IsEmail, IsNotEmpty } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class CreateUserDto {
  @IsEmail()
  @IsNotEmpty()
  @ApiProperty({
    example: 'elhoucine@aikyo.io',
    description: 'The email of the user',
  })
  email: string;

  @IsNotEmpty()
  @ApiProperty({
    example: 'password',
    description: 'The password of the user',
  })
  password: string;

  @IsNotEmpty()
  @ApiProperty({
    example: 'Elhoucine Maskour',
    description: 'The name of the user',
  })
  displayName: string;

  @IsNotEmpty()
  @ApiProperty({ example: 'admin', description: 'The role of the user' })
  role: string;
}
