import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { NestExpressApplication } from '@nestjs/platform-express';
import { resolve } from 'path';
import * as cookieParser from 'cookie-parser';
import * as csurf from 'csurf';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { ConfigService } from '@nestjs/config';
import { VersioningType } from '@nestjs/common';

async function bootstrap() {
  const csrfMiddleware = csurf({ cookie: true });
  const app = await NestFactory.create<NestExpressApplication>(AppModule);
  const configService = app.get(ConfigService);
  app.enableShutdownHooks();
  // app.setGlobalPrefix(configService.get('app.apiPrefix'), {
  //   exclude: ['/'],
  // });
  // app.enableVersioning({
  //   type: VersioningType.URI,
  // });
  app.useStaticAssets(resolve('./public'));
  app.setBaseViewsDir(resolve('./src/views'));
  app.setViewEngine('hbs');
  app.use(cookieParser());
  app.use(csrfMiddleware);

  const options = new DocumentBuilder()
    .setTitle('NestJS Firebase Auth')
    .setDescription('The NestJS Firebase Auth API description')
    .setVersion('1.0')
    .addTag('nestjs-firebase-auth')
    .build();
  const document = SwaggerModule.createDocument(app, options);
  SwaggerModule.setup('docs', app, document);
  await app.listen(configService.get('app.port'));
}
void bootstrap();
